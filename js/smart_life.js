var DAY_DELAY = 850;
Array.prototype.shuffle = function() {
  var i = this.length, j, tempi, tempj;
  if ( i == 0 ) return false;
  while ( --i ) {
     j       = Math.floor( Math.random() * ( i + 1 ) );
     tempi   = this[i];
     tempj   = this[j];
     this[i] = tempj;
     this[j] = tempi;
  }
  return this;
}


$(function(){
	$('#show-genome').tooltip({placement:'bottom', animation: true, title:'ÐÐ»Ð¸ÐºÐ½Ð¸ÑÐµ Ð¿Ð¾ ÐºÐ»ÐµÑÐºÐµ, ÑÑÐ¾Ð±Ñ ÑÐ²Ð¸Ð´ÐµÑÑ ÐµÑ Ð³ÐµÐ½Ð¾Ð¼'});
	$('#show-genome-blank').tooltip({placement:'bottom', animation: true, title:'ÐÐµÐ½Ð¾Ð¼ ÐºÐ»ÐµÑÐ¾Ðº ÐºÐ»Ð°ÑÑÐ¸ÑÐµÑÐºÐ¾Ð¹ "ÐÐ¸Ð·Ð½Ð¸" Ð²ÑÐµÐ³Ð´Ð° [0,0,0,0,0,0,0,0,0]'});
	$('#btn-start').click(startEvolution);
	$('#btn-stop').click(stopEvolution);
	$('#btn-continue').click(continueEvolution);
	$('#universe-sl').click(showGenome);
});

var uniL, uniSL;

function startEvolution()
{

	$('#btn-start').addClass('disabled');
	$('#btn-start').attr("disabled", "disabled");
	$('#btn-stop').removeClass('disabled');
	$('#btn-stop').removeAttr("disabled");
	$('#btn-continue').addClass('disabled');
	$('#btn-continue').attr("disabled", "disabled");

	var xPlaces =  $('#inputX').val()-0;
	var yPlaces = $('#inputY').val()-0;
 	var nCells = $('#inputNCells').val()-0;
 	var nActiveGenes = $('#inputNActiveGenes').val()-0;

	var pop = Population(xPlaces, yPlaces, nCells);
	var field = pop.createField(nActiveGenes);
	uniSL = Universe($('#universe-sl'), xPlaces, yPlaces, nCells, field);
	uniSL.init();
	uniSL.evolve();	
	field = pop.changeGenome(field, [0,0,0,0,0,0,0,0,0]);

	uniL = Universe($('#universe-l'), xPlaces, yPlaces, nCells, field);
	uniL.init();
	uniL.evolve();
}

function stopEvolution()
{
	$('#btn-start').removeClass('disabled');
	$('#btn-start').removeAttr("disabled");
	$('#btn-stop').addClass('disabled');
	$('#btn-stop').attr("disabled", "disabled");
	$('#btn-continue').removeClass('disabled');
	$('#btn-continue').removeAttr("disabled");
	uniL.stop();
	uniSL.stop();
}

function continueEvolution()
{
	$('#btn-start').addClass('disabled');
	$('#btn-start').attr("disabled", "disabled");
	$('#btn-stop').removeClass('disabled');
	$('#btn-stop').removeAttr("disabled");
	$('#btn-continue').addClass('disabled');
	$('#btn-continue').attr("disabled", "disabled");
	uniL.evolve();
	uniSL.evolve();
}

function showGenome(e)
{
	var x;
	var y;
	if (e.pageX != undefined && e.pageY != undefined) {
		x = e.pageX;
		y = e.pageY;
	}
	else {
		x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
		y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
	}
	x -= this.offsetLeft;
	y -= this.offsetTop;


	if(uniSL)
	{
		var gData = uniSL.getGenome(x, y);

		if(gData)
		{
			//var gStr = gDatagenome.join(', ');
			$('#show-genome-none').width('0%');
			var pr = Math.round(gData.color[0]/255*100);
			var pg = Math.round(gData.color[1]/255*100);
			var pb = 100 - pr - pg;
			$('#show-genome-r').width(pr+'%');
			$('#show-genome-g').width(pg+'%');
			$('#show-genome-b').width(pb+'%');
			$('#show-genome-text').html('['+gData.genome.join(',')+']');

		}
		else
		{
			$('#show-genome  .progress  div').width('0%');
			$('#show-genome-text').html('[?,?,?,?,?,?,?,?,?]');
		}
	}
}

/**
 * ÐÐ»Ð°ÑÑ Ð´Ð»Ñ ÑÐ°Ð±Ð¾ÑÑ ÑÐ¾ "Ð²ÑÐµÐ»ÐµÐ½Ð½Ð¾Ð¹" - Ð¿Ð¾Ð»ÐµÐ¼ Ð¸ Ð¿ÑÐ°Ð²Ð¸Ð»Ð°Ð¼Ð¸ Ð¸Ð³ÑÑ
 */
function Universe(canvas, xPlaces, yPlaces, nCells, field)
{
	var xPlaces = xPlaces;
	var yPlaces = yPlaces;
	var nCells = nCells;
	var xStep, yStep, xMax, yMax;
	var field=field;
	var fieldNew = [];
	var canvas = canvas;
	var evolving;
	var context;

	if(nCells > xPlaces * yPlaces)
	{
		return false;
	}

	/**
	 * ÐÑÑÐ¸ÑÐ¾Ð²ÐºÐ° ÑÐµÑÐºÐ¸ Ð¿Ð¾Ð»Ñ
	 */
	function drawBoard()
	{
		for (var x = xStep + 0.5; x < xMax; x += xStep) {
			context.moveTo(x, 0);
			context.lineTo(x, yMax);
		}
		for (var y = yStep + 0.5; y < yMax; y += yStep) {
			context.moveTo(0, y);
			context.lineTo(xMax, y);
		}
		context.strokeStyle = "#ccc";
		context.stroke();
	};

	/**
	 * ÐÑÑÐ¸ÑÐ¾Ð²ÐºÐ° ÐºÐ»ÐµÑÐ¾Ðº
	 */
	function drawField()
	{
		canvas.get(0).width=canvas.get(0).width;
		drawBoard();
		context.fillStyle = "#00ff00";	
		for (var pX = 0; pX < xPlaces; pX++){
			for (var pY = 0; pY < yPlaces; pY++) {
				if(field[pX][pY])
				{
					context.fillStyle = 'rgb('+field[pX][pY].color.join(',')+')';
					context.fillRect(pX*xStep, pY*yStep, xStep, yStep);
				}
			};
		}

	};

	/**
	 * ÐÐ¾Ð»ÑÑÐµÐ½Ð¸Ðµ Ð¼Ð°ÑÑÐ¸Ð²Ð° ÑÐ¾ÑÐµÐº, Ð±Ð»Ð¸Ð¶Ð°Ð¹ÑÐ¸Ñ Ðº Ð·Ð°Ð´Ð°Ð½Ð½ÑÐ¼ ÐºÐ¾Ð¾ÑÐ´Ð¸Ð½Ð°ÑÐ°Ð¼. ÐÐ´ÐµÑÑ ÐºÐ¾Ð¾ÑÐ´Ð¸Ð½Ð°ÑÑ - Ð¸Ð½Ð´ÐµÐºÑÑ Ð¼Ð°ÑÑÐ¸Ð²Ð°-Ð¿Ð¾Ð»Ñ,
	 * Ð½Ðµ Ð¿ÑÑÐ°ÑÑ Ñ ÐºÐ¾Ð¾ÑÐ´Ð¸Ð½Ð°ÑÐ°Ð¼Ð¸-Ð¿Ð¸ÐºÑÐµÐ»Ð°Ð¼Ð¸ ÑÐ¾Ð»ÑÑÐ°
	 */
	function getNearestPoints(xCenter, yCenter)
	{

 		var nearestPoints = [];
		for (var x = xCenter-1; x <= xCenter+1; x++) {
			for (var y = yCenter-1; y <= yCenter+1; y++){
				xC = x;
				yC = y;
				if(xC == xCenter && yC == yCenter)
				{
					continue;
				}
				if(xC < 0)
				{
					xC = xPlaces+xC;
				}
				if(xC >= xPlaces)
				{
					xC = xC - xPlaces;
				}

				if(yC < 0)
				{
					yC = yPlaces+yC;
				}
				if(yC >= yPlaces)
				{
					yC = yC - yPlaces;
				}
				var point={'x':xC,'y':yC};
				nearestPoints.push(point);
			}
		}
		return nearestPoints;		
	}

	/**
	 * ÐÐ¾Ð»ÑÑÐµÐ½Ð¸Ðµ Ð¼Ð°ÑÑÐ¸Ð²Ð° ÐºÐ»ÐµÑÐ¾Ðº, Ð±Ð»Ð¸Ð¶Ð°Ð¹ÑÐ¸Ñ Ðº Ð·Ð°Ð´Ð°Ð½Ð½ÑÐ¼ ÐºÐ¾Ð¾ÑÐ´Ð¸Ð½Ð°ÑÐ°Ð¼. ÐÐ´ÐµÑÑ ÐºÐ¾Ð¾ÑÐ´Ð¸Ð½Ð°ÑÑ - Ð¸Ð½Ð´ÐµÐºÑÑ Ð¼Ð°ÑÑÐ¸Ð²Ð°-Ð¿Ð¾Ð»Ñ,
	 * Ð½Ðµ Ð¿ÑÑÐ°ÑÑ Ñ ÐºÐ¾Ð¾ÑÐ´Ð¸Ð½Ð°ÑÐ°Ð¼Ð¸-Ð¿Ð¸ÐºÑÐµÐ»Ð°Ð¼Ð¸ ÑÐ¾Ð»ÑÑÐ°
	 */
	function getNearestCells(xCenter, yCenter)
	{

 		var nearestCells = [];
 		var nearestPoints = getNearestPoints(xCenter, yCenter);
 		for (p=0; p<nearestPoints.length; p++)
 		{
 			var point = nearestPoints[p];
 			if(field[point.x][point.y] !=null)
			{
				nearestCells.push(field[point.x][point.y]);
			}
		}
		return nearestCells;
	};

	/**
	 * ÐÑÐ±Ð¾Ñ Ð¿ÑÐ¸Ð²Ð»ÐµÐºÑÑÐµÐ»ÑÐ½ÑÑ ÑÐ¾ÑÐµÐº Ð¸ Ð¿ÐµÑÐµÐ¼ÐµÑÐµÐ½Ð¸Ðµ ÐºÐ»ÐµÑÐ¾Ðº
	 */
	function makeMoves()
	{
		var xs=[];
		for (var x = 0; x < xPlaces; x++) {
			xs.push(x);
		}
		xs.shuffle();
		var ys=[];
		for (var y = 0; y < yPlaces; y++) {
			ys.push(y);
		}
		ys.shuffle();

		var moveCounter = 0;
		for(var i=0; i<xs.length; i++)
		{
			for(var j=0; j<ys.length; j++)
			{
				var cellX = xs[i];
				var cellY = ys[j];
				if(field[cellX][cellY])
				{
					var vacantPoints = [];
					var nearestPoints = getNearestPoints(cellX, cellY);
					for (var p=0; p<nearestPoints.length; p++)
			 		{
			 			var point = nearestPoints[p];
			 			if(field[point.x][point.y]==null && !(cellX==point.x && cellY==point.y))
						{
							var nearestCells = getNearestCells(point.x, point.y);
							//Ð´Ð»Ñ ÐºÐ°Ð¶Ð´Ð¾Ð³Ð¾ Ð²Ð°ÐºÐ°Ð½ÑÐ½Ð¾Ð³Ð¾ Ð¼ÐµÑÑÐ° Ð±ÑÐ´ÐµÑ Ð½Ð° 1 ÑÐ¾ÑÐµÐ´Ð° Ð¼ÐµÐ½ÑÑÐµ, Ñ.Ðº. Ð¼Ñ Ð¿ÐµÑÐµÐ¼ÐµÑÑÐ¸Ð¼ÑÑ
							vacantPoints.push({'x':point.x,'y':point.y,'nearestCells': nearestCells.length-1});
						}
					}
					//Ð²Ð°ÑÐ¸Ð°Ð½Ñ "Ð¾ÑÑÐ°ÑÑÑÑ Ð½Ð° Ð¼ÐµÑÑÐµ"
					vacantPoints.push({'x':cellX,'y':cellY,'nearestCells': getNearestCells(cellX, cellY).length});

					var newPosition = CellFlyweight(field[cellX][cellY]).choosePosition(cellX, cellY, vacantPoints);
					var movingCell = field[cellX][cellY];
					field[cellX][cellY] = null
					field[newPosition.x][newPosition.y] = CellFlyweight(movingCell).setMoveCounter(moveCounter);


					moveCounter++;
				}
			}	
		}
	};
	/**
	 *   Ð Ð°ÑÑÑÐ¸ÑÐ°ÑÑ ÑÐ»ÐµÐ´ÑÑÑÐµÐµ Ð¿Ð¾ÐºÐ¾Ð»ÐµÐ½Ð¸Ðµ (Ð´ÐµÐ½Ñ): ÑÐ¼ÐµÑÑÐ¸ Ð¸ ÑÐ¾Ð¶Ð´ÐµÐ½Ð¸Ñ
	 */
	function nextDay(){

		var isChanged = false;
		var fieldNew = [];

		for (var x = 0; x < xPlaces; x++) {
			fieldNew[x]=[];
			for (var y = 0; y < yPlaces; y++){
				var nearest = getNearestCells(x, y);
				fieldNew[x][y] = field[x][y];
				if(field[x][y]==null)
				{

					if(nearest.length==3)
					{
						var lastMovedGenome = [0,0,0,0,0,0,0,0,0];
						var lastMovedNum = 0;
						for(i=0; i < nearest.length; i++)
						{
							if(nearest[i].moveCounter > lastMovedNum)
							{
								lastMovedGenome = nearest[i].genome;
								lastMovedNum = nearest[i].moveCounter;
							}
						}
						fieldNew[x][y] = CellFlyweight().createCell(lastMovedGenome);
						isChanged = true;
					}
				}
				else
				{
					if(nearest.length < 2 || nearest.length > 3)
					{
						fieldNew[x][y] = null;
						isChanged = true;
					}
				}
			}
		};

		if(!isChanged)
		{
			stop();
		}
		else
		{
			field = fieldNew;
		}
	};


	return {
		init: function(){

				context = canvas.get(0).getContext("2d");
				xStep = Math.floor(canvas.get(0).width/xPlaces);
				yStep = Math.floor(canvas.get(0).height/yPlaces);
				xMax = xStep*xPlaces;
				yMax = yStep*yPlaces;
				
				drawField();
		},
		/**
 		*   ÐÐ°ÑÐ°ÑÑ ÑÐ°Ð¾Ð»ÑÑÐ¸Ñ
 		*/
		evolve: function(){
			evolving = setInterval(function(){
				makeMoves();
				nextDay();
				drawField();
			}, DAY_DELAY);
		},
		/**
 		*   ÐÑÑÐ°Ð½Ð¾Ð²Ð¸ÑÑ ÑÐ°Ð¾Ð»ÑÑÐ¸Ñ
 		*/
		stop: function(){
			clearInterval(evolving);
		},
		/**
 		*   ÐÐ¾Ð»ÑÑÐ¸ÑÑ Ð³ÐµÐ½Ð¾Ð¼ ÐºÐ»ÐµÑÐºÐ¸ Ñ Ð·Ð°Ð´Ð°Ð½Ð½ÑÐ¼Ð¸ ÐºÐ¾Ð¾ÑÐ´Ð¸Ð½Ð°ÑÐ°Ð¼Ð¸
 		*/
		getGenome: function(x, y)
		{
			pointX = Math.floor(x/xStep);
			pointY = Math.floor(y/yStep);

			if(field[pointX] && field[pointX][pointY] && field[pointX][pointY]!=null)
			{
				return {'genome': field[pointX][pointY].genome, 'color': field[pointX][pointY].color};
			}
			return false;

		}

	};
	
};


/**
 *   ÐÐ»Ð°ÑÑ-"Ð¿ÑÐ¸ÑÐ¿Ð¾ÑÐ¾Ð±Ð»ÐµÐ½ÐµÑ" Ð´Ð»Ñ ÑÐ°Ð±Ð¾ÑÑ Ñ Ð¾ÑÐ´ÐµÐ»ÑÐ½ÑÐ¼Ð¸ ÐºÐ»ÐµÑÐºÐ°Ð¼Ð¸
 *   ÐÑÐ¸Ð½Ð¸Ð¼Ð°ÐµÑ Ð² ÐºÐ¾Ð½ÑÑÑÑÐºÑÐ¾ÑÐµ Ð½ÐµÐ¾Ð±ÑÐ·Ð°ÑÐµÐ»ÑÐ½ÑÐ¹ Ð¿Ð°ÑÐ°Ð¼ÐµÑÑ - Ð´Ð°Ð½Ð½ÑÐµ ÐºÐ»ÐµÑÐºÐ¸, Ñ ÐºÐ¾ÑÐ¾ÑÐ¾Ð¹ Ð½Ð°Ð´Ð¾ ÑÐ°Ð±Ð¾ÑÐµÑÑ.
 *	 Ð¡ÑÑÑÐºÑÑÑÐ° inputCell Ð´Ð¾Ð»Ð¶Ð½Ð° Ð±ÑÑÑ Ð°Ð½Ð°Ð»Ð¾Ð³Ð¸ÑÐ½Ð° simpleCell
 */
function CellFlyweight(inputCell)
{
	var simpleCell = {
			genome:[0,0,0,0,0,0,0,0,0],
			color: [0,0,0],
			moveCounter:0
			};
	var cell = null;
	if(inputCell)
	{
		cell = inputCell;
	}


	return{
		/**
 		*   Ð¡Ð¾Ð·Ð´Ð°Ð½Ð¸Ðµ ÐºÐ»ÐµÑÐºÐ¸.
 		*	genome - Ð¼Ð°ÑÑÐ¸Ð² Ð³ÐµÐ½Ð¾Ð¼Ð° Ð´Ð»Ñ Ð½Ð¾Ð²Ð¾Ð¹ ÐºÐ»ÐµÑÐºÐ¸. ÐÐµÐ¾Ð±ÑÐ·Ð°ÑÐµÐ»ÑÐ½ÑÐ¹ Ð¿Ð°ÑÐ°Ð¼ÐµÑÑ.
 		*	nActiveGenes - ÑÐ¸ÑÐ»Ð¾ Ð°ÐºÑÐ¸Ð²Ð½ÑÑ Ð³ÐµÐ½Ð¾Ð², ÐµÑÐ»Ð¸ Ð½ÑÐ¶Ð½Ð¾ ÑÐ¾Ð·Ð´Ð°ÑÑ ÐºÐ»ÐµÑÐºÑ ÑÐ¾ ÑÐ»ÑÑÐ°Ð¹Ð½ÑÐ¼ Ð³ÐµÐ½Ð¾Ð¼Ð¾Ð¼. ÐÐµÐ¾Ð±ÑÐ·Ð°ÑÐµÐ»ÑÐ½ÑÐ¹ Ð¿Ð°ÑÐ°Ð¼ÐµÑÑ.
 		*/
		createCell: function(genome, nActiveGenes)
		{
			var nActiveGenes = nActiveGenes || 0;
			cell = simpleCell
			var gLength = cell.genome.length;
			if(!genome)
			{
				/*for( i = 0; i < gLength; i++)
				{
					cell.genome[i] = Math.floor(Math.random() * gLength);
				}*/
				varGenesPlaced = 0;
				while(varGenesPlaced < nActiveGenes)
				{
					var gi = Math.floor(Math.random() * gLength);
					if(cell.genome[gi] ==0 )
					{
						cell.genome[gi] = 1;
						varGenesPlaced++;
					}

				}
			}
			else
			{
				cell.genome = genome;
			}

			var r = (cell.genome[0]+cell.genome[1]);
			var g = (cell.genome[2]+cell.genome[3]);
			var b = (cell.genome[4]+cell.genome[5]+cell.genome[6]+cell.genome[7]+cell.genome[8]);
			var rgbSumm = r+g+b;
			if(rgbSumm==0)
			{
				r=0;
				g=0;
				b=0;
			}
			else
			{
				r = Math.round((255/rgbSumm)*r);
				g = Math.round((255/rgbSumm)*g);
				b = Math.round((255/rgbSumm)*b);
			}
//			var r = Math.round((255/(2*gLength))*(cell.genome[0]+cell.genome[1]));
//			var g = Math.round((255/(2*gLength))*(cell.genome[2]+cell.genome[3]));
//			var b = Math.round((255/(5*gLength))*(cell.genome[4]+cell.genome[5]+cell.genome[6]+cell.genome[7]+cell.genome[8]));
			cell.color =[r, g, b];

			return cell;
		},

		/**
 		*   ÐÐ·Ð¼ÐµÐ½ÐµÐ½Ð¸Ðµ ÑÑÑÑÑÐ¸ÐºÐ° ÑÐ¾Ð´Ð¾Ð² Ð´Ð»Ñ ÐºÐ»ÐµÑÐºÐ¸
 		*	counter - Ð·Ð½Ð°ÑÐµÐ½Ð¸Ðµ ÑÑÑÑÑÐ¸ÐºÐ°
 		*/
		setMoveCounter: function(counter)
		{
			cell.moveCounter = counter;
			return cell;
		},
		/**
 		*   ÐÑÐ±Ð¾Ñ ÐºÐ»ÐµÑÐºÐ¾Ð¹ Ð¿ÑÐ¸Ð²Ð»ÐµÐºÐ°ÑÐµÐ»ÑÐ½Ð¾Ð¹ Ð´Ð»Ñ Ð¿ÐµÑÐµÐ¼ÐµÑÐµÐ½Ð¸Ñ Ð¿Ð¾Ð·Ð¸ÑÐ¸Ð¸
 		*	cellX, cellY - ÑÐµÐºÑÑÐ¸Ðµ ÐºÐ¾Ð¾ÑÐ´Ð¸Ð½Ð°ÑÑ
 		*	vacantPoints - Ð¼Ð°ÑÑÐ¸Ð² Ñ Ð´Ð°Ð½Ð½ÑÐ¼Ð¸ ÑÐ²Ð¾Ð±Ð¾Ð´Ð½ÑÑ ÑÐ¾ÑÐµÐº
 		*/
		choosePosition: function(cellX, cellY, vacantPoints)
		{
			var bestX = cellX;
			var bestY = cellY;
			var bestIndex = 0;
			vacantPoints.shuffle();
			for (var i=0; i<vacantPoints.length; i++)
			{
				var gene = vacantPoints[i].nearestCells;
				if(cell.genome[gene] > bestIndex)
				{
					bestX=vacantPoints[i].x;
					bestY=vacantPoints[i].y;
				}
			}	
			return {'x':bestX, 'y':bestY};
		}		
	}
}


/**
 *   Ð Ð°Ð±Ð¾ÑÐ° Ñ Ð¿Ð¾Ð¿ÑÐ»ÑÐ¸ÐµÐ¹ Ð½Ð° Ð¿Ð¾Ð»Ðµ ÐºÐ°Ðº Ñ ÑÐµÐ»ÑÐ¼
 */
function Population(xPlaces, yPlaces, nCells)
{
	var xPlaces = xPlaces;
	var yPlaces = yPlaces;
	var nCells = nCells;

	return {
		/**
 		*   Ð¡Ð¾Ð·Ð´Ð°ÑÑ Ð¿Ð¾Ð¿ÑÐ»ÑÑÐ¸Ñ ÑÐ¾ ÑÐ»ÑÑÐ°Ð¹Ð½ÑÐ¼ Ð³ÐµÐ½Ð¾Ð¼Ð¾Ð¼
 		*	nActiveGenes - ÑÐ¸ÑÐ»Ð¾ Ð°ÐºÑÐ¸Ð²Ð½ÑÑ Ð³ÐµÐ½Ð¾Ð². ÐÐµÐ¾Ð±ÑÐ·Ð°ÑÐµÐ»ÑÐ½ÑÐ¹ Ð¿Ð°ÑÐ°Ð¼ÐµÑÑ.
 		*/
		createField: function(nActiveGenes)
		{
			var field = [];
			var nActiveGenes = nActiveGenes || 0;

			for (var x = 0; x < xPlaces; x++) {
				field[x] = [];
				for (var y = 0; y < yPlaces; y++){
					field[x][y] = null;	
				}
			};

			cellsPlaced = 0;
			nMisses = 0;
			while(cellsPlaced < nCells || nMisses > xPlaces * yPlaces)
			{
				var x = Math.floor(Math.random() * xPlaces);
				var y = Math.floor(Math.random() * yPlaces);
				if(field[x][y]===null)
				{
					field[x][y] = CellFlyweight().createCell(null, nActiveGenes);
					cellsPlaced++;
				}
				else
				{
					nMisses++;
				}
			}
/*
				field[0][0] = CellFlyweight().createCell();
				field[1][1] = CellFlyweight().createCell();
				field[2][1] = CellFlyweight().createCell();
				field[0][2] = CellFlyweight().createCell();
				field[1][2] = CellFlyweight().createCell();
*/
			return field;
		},

		/**
 		*   Ð¡Ð¾Ð·Ð´Ð°ÑÑ ÐºÐ»Ð¾Ð½ Ð¿ÐµÑÐµÐ´Ð°Ð½Ð½Ð¾Ð¹ Ð¿Ð¾Ð¿ÑÐ»ÑÑÐ¸Ð¸ Ñ Ð·Ð°Ð´Ð°Ð½Ð½ÑÐ¼ Ð³ÐµÐ½Ð¾Ð¼Ð¾Ð¼
 		*/
		changeGenome: function(field, genome)
		{
			var field = field;
			var newField = []
			for (var x = 0; x < xPlaces; x++) {
				newField[x] = []
				for (var y = 0; y < yPlaces; y++){
					if(field[x][y] != null)
					{
						newField[x][y] = CellFlyweight().createCell(genome);
					}
					else
					{
						newField[x][y] = null;
					}

				}
			};
			return newField;
		}
	}
}